<?php

/*
 * This file is part of the Snooper symfony package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\SnooperBridgeBundle;

use Snooper\SnooperBridgeBundle\DependencyInjection\Compiler\TrackingPass;
use Snooper\SnooperBridgeBundle\DependencyInjection\Compiler\TwigThemePass;
use Symfony\Component\DependencyInjection\Compiler\PassConfig;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class SnooperBridgeBundle
 * @package Snooper\SnooperBridgeBundle
 */
class SnooperBridgeBundle extends Bundle
{
    const VERSION_MAJOR = 1;

    const VERSION_MINOR = 5;

    const VERSION_REV = 0;

    const VERSION_ID = 10504;

    /**
     * @return string
     */
    public static function getVersion()
    {
        return sprintf('%s.%s.%s', self::VERSION_MAJOR, self::VERSION_MINOR,
            self::VERSION_REV);
    }

    /**
     * @param ContainerBuilder $container
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new TrackingPass(),PassConfig::TYPE_OPTIMIZE);
        $container->addCompilerPass(new TwigThemePass(),PassConfig::TYPE_OPTIMIZE);
    }
}
