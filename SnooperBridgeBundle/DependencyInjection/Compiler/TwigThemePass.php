<?php

/*
 * This file is part of the Snooper symfony package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\SnooperBridgeBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Class TwigThemePass
 * @package Snooper\SnooperBridgeBundle\DependencyInjection\Compiler
 */
class TwigThemePass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('twig.loader.native_filesystem')) {
            return;
        }

        $path = realpath(__DIR__.'/../../Resources/views');
        $loader = $container->getDefinition('twig.loader.native_filesystem');
        $loader->addMethodCall('addPath', [$path,'Snooper']);
    }
}
