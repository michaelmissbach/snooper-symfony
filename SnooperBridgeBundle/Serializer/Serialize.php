<?php

/*
 * This file is part of the Snooper symfony package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\SnooperBridgeBundle\Serializer;

use Snooper\Components\Serialize\ISerializer;
use Symfony\Component\Serializer\Serializer;

/**
 * Class Serialize
 * @package Snooper\SnooperBridgeBundle\Serializer
 */
class Serialize implements ISerializer
{
    /**
     * @var Serializer
     */
    protected $serializer;

    /**
     * Serialize constructor.
     * @param $serializer
     */
    public function __construct($serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @return bool
     */
    public function symfonySerializerIsExistingAndSupportsEncoding()
    {
        return is_object($this->serializer) && $this->serializer->supportsEncoding('json');
    }

    /**
     * @param $subject
     * @return string
     */
    public function serialize($subject)
    {
        return $this->serializer->serialize($subject,'json');
    }
}
