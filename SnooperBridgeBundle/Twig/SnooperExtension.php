<?php

/*
 * This file is part of the Snooper symfony package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\SnooperBridgeBundle\Twig;

use Snooper\Components\Snooper;
use Snooper\SnooperBridgeBundle\SnooperBridgeBundle;
use Symfony\Component\Stopwatch\Stopwatch;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class SnooperExtension
 * @package Snooper\SnooperBridgeBundle\Twig
 */
class SnooperExtension extends AbstractExtension
{
    /**
     * @var Stopwatch
     */
    protected $stopwatch;

    /**
     * SnooperExtension constructor.
     * @param Stopwatch $stopwatch
     */
    public function __construct(Stopwatch $stopwatch)
    {
        $this->stopwatch = $stopwatch;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'snooper.tracking.twig';
    }

    /**
     * @return array|\Twig_SimpleFunction[]
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('print_snooper_section', array($this, 'getSection'), array('is_safe' => array('html'))),
            new TwigFunction('print_snooper_component_version', array($this, 'getComponentVersion'), array('is_safe' => array('html'))),
            new TwigFunction('print_snooper_bridge_version', array($this, 'getBridgeVersion'), array('is_safe' => array('html')))
        ];
    }

    /**
     * @param $section
     * @return string
     * @throws \Exception
     */
    public function getSection($section)
    {
        $this->stopwatch->start(sprintf('Snooper Compiler/Renderer "%s"',$section));

        if ($section === Snooper::instance()->getConfig()->getFirstSection()) {
            $compiler = Snooper::instance()->getCompiler();
            $path = realpath(__DIR__.'/../Resources/views/script/');
            $compiler->addPreCompileAdditionalTemplates(
                file_get_contents($path.'/symfony_script.tpl')
            );
        }
        $content = Snooper::instance()->outputSection($section);

        $this->stopwatch->stop(sprintf('Snooper Compiler/Renderer "%s"',$section));

        return $content;
    }

    /**
     * @return string
     */
    public function getComponentVersion()
    {
        return Snooper::getVersion();
    }

    /**
     * @return string
     */
    public function getBridgeVersion()
    {
        return SnooperBridgeBundle::getVersion();
    }
}
