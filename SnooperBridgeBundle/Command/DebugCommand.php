<?php

/*
 * This file is part of the Snooper symfony package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\SnooperBridgeBundle\Command;

use Snooper\Components\Log\ILog;
use Snooper\Components\Log\LoggerContainer;
use Snooper\Components\Provider\ProviderCallContainer;
use Snooper\Components\Snooper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class DebugCommand
 * @package Snooper\SnooperBridgeBundle\Command
 */
class DebugCommand extends AbstractDebugCommand
{
    /**
     *
     */
    protected function configure()
    {
        parent::configure();

        $this->setName('snooper:check');

        $this->setDescription('Prints debug informations.');

        $this->addArgument('event', InputArgument::REQUIRED, 'Specify event name to raise.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->printOutputHeader($output);

        try {

            Snooper::instance()->getConfig()->setDebugMode(true);

            $eventName = $input->getArgument('event');

            Snooper::instance()->raiseEvent($eventName,[]);

            $output->writeln('List of called providers:');
            $output->writeln('');

            $output->writeln(
                $this->formatToExactCharCount('Class',60).
                $this->formatToExactCharCount(' Method',40)
            );
            $output->writeln($this->formatToExactCharCount('',self::MAX_CHARS_IN_LINE, '='));

            foreach (ProviderCallContainer::instance()->getDeepYielded() as $eventCall) {
                $output->writeln(
                    $this->formatToExactCharCount(get_class($eventCall->getProvider()),60).
                    $this->formatToExactCharCount(' '.$eventCall->getMethod(),40)
                );
            }
            $output->writeln($this->formatToExactCharCount('',self::MAX_CHARS_IN_LINE, '='));
            $output->writeln('');

            $output->writeln('Log:');
            $output->writeln('');

            $output->writeln(
                $this->formatToExactCharCount('Time',10).
                $this->formatToExactCharCount('Message',90)
            );
            $output->writeln($this->formatToExactCharCount('',self::MAX_CHARS_IN_LINE, '='));

            /** @var ILog $log */
            foreach (LoggerContainer::instance()->getRaw() as $list) {
                foreach ($list as $log) {
                    $output->writeln(
                        $this->formatToExactCharCount((round($log->getExecutionTime(),3) / 1000). 'ms',10).
                        $this->formatToExactCharCount(' '.$log->getMessage(),90)
                    );
                }
            }
            $output->writeln($this->formatToExactCharCount('',self::MAX_CHARS_IN_LINE, '='));

        } catch(\Exception $e) {
            $output->writeln($e->getMessage());
        }

        $this->printOutputFooter($output);
    }
}
