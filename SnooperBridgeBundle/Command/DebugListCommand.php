<?php

/*
 * This file is part of the Snooper symfony package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\SnooperBridgeBundle\Command;

use Snooper\Components\Provider\IProvider;
use Snooper\Components\Provider\ProviderContainer;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class DebugListCommand
 * @package Snooper\SnooperBridgeBundle\Command
 */
class DebugListCommand extends AbstractDebugCommand
{
    /**
     *
     */
    protected function configure()
    {
        parent::configure();

        $this->setName('snooper:list');

        $this->setDescription('Prints debug informations.');

    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->printOutputHeader($output);

        try {
            $events = [];

            /** @var IProvider $provider */
            foreach (ProviderContainer::instance()->getRaw() as $provider) {
                foreach ($provider->getEvents() as $eventName=>$eventConfig) {
                    if (!array_key_exists($eventName, $events)) {
                        $events[$eventName] = [];
                    }
                    $events[$eventName][] = get_class($provider);
                }
            }

            if (count($events)) {
                $output->writeln('List of events:');
                $output->writeln('');

                $output->writeln(
                    $this->formatToExactCharCount('Event name',30).
                    $this->formatToExactCharCount(' Provider',70)
                );
                $output->writeln($this->formatToExactCharCount('',self::MAX_CHARS_IN_LINE, '='));

                foreach ($events as $eventName=>$eventProviders) {
                    $first = true;
                    foreach ($eventProviders as $eventProvider) {
                        if (!$first) {
                            $eventName = '';
                        }
                        $output->writeln(
                            $this->formatToExactCharCount($eventName,30).
                            $this->formatToExactCharCount(' '.$eventProvider,70)
                        );
                        $first = false;
                    }
                }

                $output->writeln($this->formatToExactCharCount('',self::MAX_CHARS_IN_LINE, '='));
                $output->writeln('');
            } else {
                $output->writeln('No events registered.');
            }


        } catch(\Exception $e) {
            $output->writeln($e->getMessage());
        }

        $this->printOutputFooter($output);
    }
}
