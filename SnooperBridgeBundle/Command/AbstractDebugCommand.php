<?php

/*
 * This file is part of the Snooper symfony package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\SnooperBridgeBundle\Command;

use Snooper\Components\Snooper;
use Snooper\SnooperBridgeBundle\EventListener\Events;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class AbstractDebugCommand
 * @package Snooper\SnooperBridgeBundle\Command
 */
class AbstractDebugCommand extends Command
{
    /**
     * @var integer
     */
    const MAX_CHARS_IN_LINE = 100;

    /**
     * AbstractDebugCommand constructor.
     * @param Events $eventListener
     */
    public function __construct(Events $eventListener)
    {
        $eventListener->run();
        parent::__construct();
    }
    
    /**
     * @param $input
     * @param $charCount
     * @param string $fillChar
     * @return bool|string
     */
    protected function formatToExactCharCount($input,$charCount,$fillChar = ' ')
    {
        $length = strlen($input);
        if ($length > $charCount) {
            return '...'.substr($input,strlen($input) - ($charCount - 3),$charCount);
        }

        while($length < $charCount) {
            $input .= $fillChar;
            $length = strlen($input);
        }

        return $input;
    }

    /**
     * @param OutputInterface $output
     */
    protected function printOutputHeader(OutputInterface $output)
    {
        $output->writeln($this->formatToExactCharCount('',self::MAX_CHARS_IN_LINE,'*'));
        $output->writeln(sprintf('Snooper (Version %s)',Snooper::getVersion()));
        $output->writeln($this->formatToExactCharCount('',self::MAX_CHARS_IN_LINE,'*'));
        $output->writeln('');
    }
    
    /**
     * @param OutputInterface $output
     */
    protected function printOutputFooter(OutputInterface $output)
    {
        $output->writeln('');
        $output->writeln($this->formatToExactCharCount('',self::MAX_CHARS_IN_LINE,'*'));
        $output->writeln(sprintf('Copyright Michael Mißbach %s.',Snooper::COPYRIGHT_YEAR));
        $output->writeln($this->formatToExactCharCount('',self::MAX_CHARS_IN_LINE,'*'));
        $output->writeln('');
    }
}
