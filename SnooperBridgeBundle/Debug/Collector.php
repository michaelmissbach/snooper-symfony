<?php

/*
 * This file is part of the Snooper symfony package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\SnooperBridgeBundle\Debug;

use Snooper\Components\Compiler\CompiledContainer;
use Snooper\Components\Log\LoggerContainer;
use Snooper\Components\Provider\ProviderContainer;
use Snooper\Components\Snooper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\DataCollector\DataCollector;

/**
 * Class Collector
 * @package Snooper\SnooperBridgeBundle\Debug
 */
class Collector extends DataCollector
{
    /**
     * Collects data for the given Request and Response.
     *
     * @param Request $request A Request instance
     * @param Response $response A Response instance
     * @param \Throwable $exception An Exception instance
     */
    public function collect(Request $request, Response $response, \Throwable $exception = null)
    {
        $this->data = [];

        $timeline = [];
        foreach (LoggerContainer::instance()->getRaw() as $group=>$values) {
            if (!array_key_exists($group, $timeline)) {
                $timeline[$group] = [];
            }
            $timeline[$group] = [
                'data' => $values,
                'min' => $values[0]->getExecutionTime(),
                'max' => end($values)->getExecutionTime()
            ];
        }
        $this->data['timeline'] = $timeline;

        $events     = [];
        $providers  = [];
        foreach (ProviderContainer::instance()->getRaw() as $provider) {
            $providerClass = get_class($provider);
            $providers[] = ['name'=>$providerClass,'active'=>$provider->isActive()];
            if ($provider->isActive()) {
                foreach ($provider->getEvents() as $eventName=>$eventConfig) {
                    if (!array_key_exists($eventName, $events)) {
                        $events[$eventName] = [];
                    }
                    $events[$eventName][] = $providerClass;
                }
            }
        }
        $this->data['events']       = $events;
        $this->data['providers']    = $providers;

        $this->data['isdebug'] = Snooper::instance()->getConfig()->isDebugMode();

        $this->data['sections'] =
            array_merge(
                CompiledContainer::instance()->getRaw(),
                Container::instance()->getRaw()
            );
    }

    /**
     * @return mixed
     */
    public function isDebug()
    {
        return $this->data['isdebug'];
    }

    /**
     * @return mixed
     */
    public function getTimeline()
    {
        return $this->data['timeline'];
    }

    /**
     * @return mixed
     */
    public function getEvents()
    {
        return $this->data['events'];
    }

    /**
     * @return mixed
     */
    public function getProviders()
    {
        return $this->data['providers'];
    }

    /**
     * @return mixed
     */
    public function getSections()
    {
        return $this->data['sections'];
    }

    /**
     * Returns the name of the collector.
     *
     * @return string The collector name
     */
    public function getName()
    {
        return 'snooper-collector-tracking';
    }

    /**
     *
     */
    public function reset()
    {
    }
}
