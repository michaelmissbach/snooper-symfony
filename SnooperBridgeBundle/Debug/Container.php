<?php

/*
 * This file is part of the Snooper symfony package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\SnooperBridgeBundle\Debug;

use Snooper\Components\Utility\Traits\Container\ContainerGrouped;
use Snooper\Components\Utility\Traits\Singleton;

/**
 * Class Container
 * @package Snooper\SnooperBridgeBundle\Debug
 */
class Container
{
    use Singleton;
    use \Snooper\Components\Utility\Traits\Container\Container;
    use ContainerGrouped;

    /**
     * @param $group
     * @param $list
     */
    public function addList($group,$list)
    {
        foreach($list as $key=>$values) {
            if (count($values)) {
                $this->add(
                    $group,
                    sprintf('%s:',strtoupper($key))
                );
                foreach ($values as $value) {
                    $this->add(
                        $group,
                        json_encode($value)
                    );
                }
            }
        }
    }
}
