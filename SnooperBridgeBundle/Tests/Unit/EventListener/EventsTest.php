<?php

/*
 * This file is part of the Snooper symfony package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\SnooperBridgeBundle\Tests\Unit\EventListener;

use App\Kernel;
use PHPUnit\Framework\TestCase;
use Snooper\Components\Config\Config;
use Snooper\Components\Provider\ProviderContainer;
use Snooper\Components\Snooper;
use Snooper\Components\Tests\Fixtures\Provider\Provider;
use Snooper\SnooperBridgeBundle\Event\SnooperEvent;
use Snooper\SnooperBridgeBundle\EventListener\Events;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Stopwatch\Stopwatch;

/**
 * Class EventsTest
 * @package Snooper\SnooperBridgeBundle\Tests\Unit\EventListener
 */
class EventsTest extends TestCase
{
    /**
     * @return Config
     */
    protected function getConfig()
    {
        return new class extends Config {

            protected $rawParameters = [
                'first_section' => 'head',
                'last_section' => 'footer',
                'sections' => [
                    'head','body','footer'
                ],
                'json_response_key' => 'snooper_data',
                'xhr_send_mode'=>self::XHR_SEND_METHOD_BOTH,
                'debug' => [
                    'nested_level' => 25,
                    'url_param' => 'snooper_debug',
                    'session_param' => 'snooper_debug',
                    'debug_environments' => [
                        'dev'
                    ]
                ],
                'log' => [
                    'excludeClass' => [
                        'Doctrine\ORM\PersistentCollection'
                    ]
                ],
                'ignoredRoutes' => [
                    '/\/admin$/'
                ]
            ];
        };
    }

    public function testAddProvider()
    {
        $provider = new Provider();
        Snooper::instance()->reset();
        $events = new Events(null,new Stopwatch());
        $events->addProvider($provider);
        $this->assertEquals($provider,ProviderContainer::instance()->getRaw()[0]);
    }

    public function testRaiseEvent()
    {
        $provider = new Provider();
        Snooper::instance()->reset();
        Snooper::instance()->setConfig($this->getConfig());
        Snooper::instance()->addProvider($provider);

        $events = new Events(null,new Stopwatch());
        $events->raiseEvent(SnooperEvent::create('eventStandardResponse',[]));

        $result = \Snooper\Components\Response\Container::instance()->getRaw();
        $this->assertTrue($result[0] instanceof \Snooper\Components\Response\Standard\Response);
        $this->assertEquals($result[0]->getJavascriptMethodName(),'console.log');

        Snooper::instance()->reset();
    }

    public function testIsIgnoredRoute()
    {
        Snooper::instance()->reset();
        Snooper::instance()->setConfig($this->getConfig());

        $events = new class(null,new Stopwatch()) extends Events {

            public function testIsIgnoredRoute($path)
            {
                return $this->isIgnoredRoute($path);
            }
        };

        $this->assertTrue($events->testIsIgnoredRoute('/admin'));
        $this->assertNotTrue($events->testIsIgnoredRoute('/'));
    }

    public function testAddToJsonContent()
    {
        Snooper::instance()->reset();
        Snooper::instance()->setConfig($this->getConfig());
        $config = Snooper::instance()->getConfig();

        $events = new class(null,new Stopwatch()) extends Events {

            public function testAddToJsonContent(JsonResponse $response,$list)
            {
                $this->addToJsonContent($response,$list);
            }
        };

        $list = ['listkey1'=>'listvalue1'];
        $response = new JsonResponse();
        $events->testAddToJsonContent($response,$list);

        $this->assertEquals(sprintf('{"%s":{"listkey1":"listvalue1"}}',$config->getJsonResponseKey()),$response->getContent());
    }

    public function testAddToArray()
    {
        Snooper::instance()->reset();
        Snooper::instance()->setConfig($this->getConfig());
        $config = Snooper::instance()->getConfig();

        $events = new class(null,new Stopwatch()) extends Events {

            public function testAddToArray(&$content,$list)
            {
                $this->addToArray($content,$list);
            }
        };

        $content = ['key1'=>'value1'];
        $list = ['listkey1'=>'listvalue1'];
        $result = ['key1'=>'value1',$config->getJsonResponseKey()=> ['listkey1'=>'listvalue1']];

        $events->testAddToArray($content,$list);
        $this->assertEquals($content,$result);
    }

    public function testAddToObject()
    {
        Snooper::instance()->reset();
        Snooper::instance()->setConfig($this->getConfig());
        $config = Snooper::instance()->getConfig();

        $events = new class(null,new Stopwatch()) extends Events {

            public function testAddToObject(&$content,$list)
            {
                $this->addToStdClass($content,$list);
            }
        };

        $content = new \stdClass();
        $content->key1='value1';
        $list = ['listkey1'=>'listvalue1'];

        $result = new \stdClass();
        $result->key1 = 'value1';
        $result->{$config->getJsonResponseKey()}= ['listkey1'=>'listvalue1'];

        $events->testAddToObject($content,$list);
        $this->assertEquals($content,$result);
    }

    public function testOnKernelRequest()
    {
        Snooper::instance()->reset();
        Snooper::instance()->setConfig($this->getConfig());
        $config = Snooper::instance()->getConfig();

        $events = new class(null,new Stopwatch()) extends Events {

            /**
             * @var ParameterBag
             */
            public $__session;

            protected function getSession()
            {
                $this->__session = new ParameterBag();
                return $this->__session;
            }

        };
        $request = new Request();
        $kernel = new Kernel('test',true);

        $events->onKernelRequest(new GetResponseEvent($kernel,$request, HttpKernelInterface::MASTER_REQUEST));
        $this->assertNotTrue($events->__session->has($config->getDebugSessionParam()));
        $this->assertNotTrue($events->__session->get($config->getDebugSessionParam()));

        $request->query->set($config->getDebugUrlParam(),1);
        $events->onKernelRequest(new GetResponseEvent($kernel,$request, HttpKernelInterface::MASTER_REQUEST));
        $this->assertTrue($events->__session->has($config->getDebugSessionParam()));
        $this->assertTrue($events->__session->get($config->getDebugSessionParam()));

        Snooper::instance()->reset();
    }

    public function testOnKernelResponse()
    {
        $provider = new Provider();
        Snooper::instance()->reset();
        Snooper::instance()->addProvider($provider);

        $request = new Request();
        $kernel = new Kernel('test',true);

        Snooper::instance()->setConfig($this->getConfig());
        $config = Snooper::instance()->getConfig();

        $response = new JsonResponse();
        $events = new Events(null,new Stopwatch());
        $events->raiseEvent(SnooperEvent::create('eventStandardResponse',[]));
        $events->onKernelResponse(new FilterResponseEvent($kernel,$request,HttpKernelInterface::MASTER_REQUEST,$response));
        $this->assertEquals(sprintf('{"%s":[{"type":"standard","method":"console.log","params":[{"param":"hello world from m1","isRawEvalNeededParam":false}]}]}',$config->getJsonResponseKey()),$response->getContent());
        $this->assertEquals('[{"type":"standard","method":"console.log","params":[{"param":"hello world from m1","isRawEvalNeededParam":false}]}]',$response->headers->get($config->getJsonResponseKey()));

        $response = new Response();
        $events = new Events(null,new Stopwatch());
        $events->raiseEvent(SnooperEvent::create('eventStandardResponse',[]));
        $events->onKernelResponse(new FilterResponseEvent($kernel,$request,HttpKernelInterface::MASTER_REQUEST,$response));
        $this->assertEquals('',$response->getContent());
        $this->assertEquals('[{"type":"standard","method":"console.log","params":[{"param":"hello world from m1","isRawEvalNeededParam":false}]}]',$response->headers->get($config->getJsonResponseKey()));
    }
}
