<?php

/*
 * This file is part of the Snooper symfony package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\SnooperBridgeBundle\Tests\Unit\Twig;

use PHPUnit\Framework\TestCase;
use Snooper\Components\Config\Config;
use Snooper\Components\Snooper;
use Snooper\Components\Utility\StringUtility;
use Snooper\SnooperBridgeBundle\SnooperBridgeBundle;
use Snooper\SnooperBridgeBundle\Twig\SnooperExtension;
use Symfony\Component\Stopwatch\Stopwatch;

/**
 * Class SnooperExtensionTest
 * @package Snooper\SnooperBridgeBundle\Tests\Unit\Twig
 */
class SnooperExtensionTest extends TestCase
{
    protected function getConfig()
    {
        return new class extends Config {

            protected $rawParameters = [
                'first_section' => 'head',
                'last_section' => 'footer',
                'sections' => [
                    'head','body','footer'
                ],
                'json_response_key' => 'snooper_data',
                'xhr_send_mode'=>self::XHR_SEND_METHOD_BOTH,
                'debug' => [
                    'nested_level' => 25,
                    'url_param' => 'snooper_debug',
                    'session_param' => 'snooper_debug',
                    'debug_environments' => [
                        'dev'
                    ]
                ],
                'log' => [
                    'excludeClass' => [
                        'Doctrine\ORM\PersistentCollection'
                    ]
                ],
                'ignoredRoutes' => [
                    '/\/admin$/'
                ]
            ];
        };
    }

    public function testGetSections()
    {
        Snooper::instance()->reset();
        Snooper::instance()->setConfig($this->getConfig());

        $twig = new SnooperExtension(new Stopwatch());

        $this->assertTrue(StringUtility::startsWith($twig->getSection('head'),'<script type="text/javascript">'));
        $this->assertEquals('',$twig->getSection('body'));
        $this->assertEquals('',$twig->getSection('footer'));

        Snooper::instance()->reset();
    }

    public function testGetName()
    {
        $twig = new SnooperExtension(new Stopwatch());
        $this->assertEquals('snooper.tracking.twig',$twig->getName());
    }

    public function testGetFunctions()
    {
        $twig = new SnooperExtension(new Stopwatch());
        $this->assertEquals(3,count($twig->getFunctions()));
    }

    public function testGetVersion()
    {
        $twig = new SnooperExtension(new Stopwatch());
        $this->assertEquals(SnooperBridgeBundle::getVersion(),$twig->getBridgeVersion());
    }
}
