<?php

/*
 * This file is part of the Snooper symfony package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\SnooperBridgeBundle\Tests\Unit\Event;

use PHPUnit\Framework\TestCase;
use Snooper\SnooperBridgeBundle\Event\SnooperEvent;

/**
 * Class SnooperEventTest
 * @package Snooper\SnooperBridgeBundle\Tests\Unit\Event
 */
class SnooperEventTest extends TestCase
{
    public function testGetParameters()
    {
        $params = ['test1','test2','test3'];
        $event = SnooperEvent::create('test-name',$params);
        $this->assertEquals('test-name',$event->getEventName());
        $this->assertEquals($params,$event->getParameters());
    }
}
