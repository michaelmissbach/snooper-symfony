<?php

/*
 * This file is part of the Snooper symfony package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\SnooperBridgeBundle\Tests\Unit\Event;

use PHPUnit\Framework\TestCase;
use Snooper\Components\Config\Config;
use Snooper\SnooperBridgeBundle\Event\SnooperConfigEvent;

/**
 * Class SnooperConfigEventTest
 * @package Snooper\SnooperBridgeBundle\Tests\Unit\Event
 */
class SnooperConfigEventTest extends TestCase
{
    public function testGetConfig()
    {
        $config = new Config();
        $event = SnooperConfigEvent::create($config);
        $this->assertEquals($config,$event->getConfig());
    }
}
