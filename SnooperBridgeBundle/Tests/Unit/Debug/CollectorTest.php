<?php

/*
 * This file is part of the Snooper symfony package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\SnooperBridgeBundle\Tests\Unit\Debug;

use PHPUnit\Framework\TestCase;
use Snooper\Components\Log\LoggerContainer;
use Snooper\Components\Snooper;
use Snooper\Components\Tests\Fixtures\Provider\Provider;
use Snooper\SnooperBridgeBundle\Debug\Collector;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CollectorTest
 * @package Snooper\SnooperBridgeBundle\Tests\Unit\Debug
 */
class CollectorTest extends TestCase
{
    public function testCollect()
    {
        Snooper::instance()->reset();
        $provider = new Provider();
        Snooper::instance()->addProvider($provider);
        LoggerContainer::instance()->add('test','message1');

        $collector = new Collector();
        $collector->collect(new Request(), new Response());

        $timeline = $collector->getTimeline();
        $this->assertTrue(array_key_exists('Processor',$timeline));
        $this->assertNotTrue($collector->isDebug());
        $this->assertEquals(['eventStandardResponse','eventInteractionResponse','eventMixed'],array_keys($collector->getEvents()));
        $this->assertEquals('Snooper\Components\Tests\Fixtures\Provider\Provider',$collector->getProviders()[0]['name']);
        $this->assertEquals('snooper-collector-tracking',$collector->getName());
        $this->assertEquals(0,count($collector->getSections()));

        Snooper::instance()->reset();
    }
}
