<?php

/*
 * This file is part of the Snooper symfony package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\SnooperBridgeBundle\Tests\Unit\Command;

use PHPUnit\Framework\TestCase;
use Snooper\Components\Snooper;
use Snooper\Components\Tests\Fixtures\Provider\Provider;
use Snooper\SnooperBridgeBundle\Command\DebugListCommand;
use Snooper\SnooperBridgeBundle\EventListener\Events;
use Symfony\Component\Console\Formatter\OutputFormatter;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * Class DebugListCommandTest
 * @package Snooper\SnooperBridgeBundle\Tests\Unit\Command
 */
class DebugListCommandTest extends TestCase
{
    public function testSimple()
    {
        Snooper::instance()->reset();
        Snooper::instance()->addProvider(new Provider());
        $commandtester = new CommandTester(new DebugListCommand(new Events(new \stdClass(),new \stdClass())));
        $result = $commandtester->execute([]);
        $this->assertEquals(0,$result);
        Snooper::instance()->reset();
    }
}
