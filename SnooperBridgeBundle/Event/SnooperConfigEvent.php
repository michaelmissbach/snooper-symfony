<?php

/*
 * This file is part of the Snooper symfony package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\SnooperBridgeBundle\Event;

use Snooper\Components\Config\IConfig;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Class SnooperConfigEvent
 * @package Snooper\SnooperBridgeBundle\Event
 */
class SnooperConfigEvent extends Event
{
    /**
     * @var IConfig
     */
    protected $config;

    /**
     * SnooperLoadEvent constructor.
     * @param IConfig $config
     */
    protected function __construct(IConfig $config)
    {
        $this->config = $config;
    }

    /**
     * @param IConfig $config
     * @return SnooperConfigEvent
     */
    public static function create(IConfig $config)
    {
        return new static($config);
    }

    /**
     * @return IConfig
     */
    public function getConfig()
    {
        return $this->config;
    }
}
