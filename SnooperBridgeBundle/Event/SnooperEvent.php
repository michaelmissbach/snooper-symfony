<?php

/*
 * This file is part of the Snooper symfony package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\SnooperBridgeBundle\Event;

use Snooper\Components\Utility\Bag;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Class SnooperEvent
 * @package Snooper\SnooperBridgeBundle\Event
 */
class SnooperEvent extends Event
{
    /**
     * @var Bag
     */
    protected $parameters;

    /**
     * @var string
     */
    protected $eventName;

    /**
     * SymfonyEvent constructor.
     * @param $eventName
     * @param array $parameters
     */
    protected function __construct($eventName, $parameters = [])
    {
        $this->eventName = trim(strtolower($eventName));
        $this->parameters = $parameters;
    }

    /**
     * @param $eventName
     * @param array $parameters
     * @return SnooperEvent
     */
    public static function create($eventName, $parameters = [])
    {
        return new static($eventName,$parameters);
    }

    /**
     * @return Bag
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @return string
     */
    public function getEventName()
    {
        return $this->eventName;
    }
}
