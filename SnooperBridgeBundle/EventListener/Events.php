<?php

/*
 * This file is part of the Snooper symfony package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\SnooperBridgeBundle\EventListener;

use Snooper\Components\Config\Config;
use Snooper\Components\Log\LoggerContainer;
use Snooper\Components\Provider\IProvider;
use Snooper\Components\Snooper;
use Snooper\SnooperBridgeBundle\Debug\Container;
use Snooper\SnooperBridgeBundle\Event\SnooperEvent;
use Snooper\SnooperBridgeBundle\Event\SnooperConfigEvent;
use Snooper\SnooperBridgeBundle\Serializer\Serialize;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Stopwatch\Stopwatch;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Class Events
 * @package Snooper\SnooperBridgeBundle\EventListener
 */
class Events
{
    const SNOOPER_CONFIG = 'snooper.config';
    const SNOOPER_EVENT = 'snooper.event';

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var Serialize|null
     */
    protected $serializer;

    /**
     * @var Stopwatch
     */
    protected $stopwatch;

    /**
     * @var EventDispatcherInterface
     */
    protected $eventDispatcher;

    /**
     * @var KernelInterface
     */
    protected $kernel;

    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * Events constructor.
     * @param ContainerInterface $container
     * @param Stopwatch $stopwatch
     * @param EventDispatcherInterface $eventDispatcher
     * @param KernelInterface $kernel
     * @param RequestStack $requestStack
     */
    public function __construct(
        ContainerInterface $container,
        Stopwatch $stopwatch,
        EventDispatcherInterface $eventDispatcher,
        KernelInterface $kernel,
        RequestStack $requestStack
    )
    {
        $this->container       = $container;
        $this->stopwatch       = $stopwatch;
        $this->eventDispatcher = $eventDispatcher;
        $this->kernel          = $kernel;
        $this->requestStack    = $requestStack;
    }

    /**
     * @param $serializer
     */
    public function setSerializer($serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     *
     */
    public function run()
    {
        if ($this->serializer && $this->serializer->symfonySerializerIsExistingAndSupportsEncoding()) {
            Snooper::instance()->setSerializer($this->serializer);
        }

        $this->eventDispatcher
            ->dispatch(SnooperConfigEvent::create(Snooper::instance()->getConfig()),self::SNOOPER_CONFIG);
    }

    /**
     * @internal
     *
     * @param IProvider $provider
     */
    public function addProvider(IProvider $provider)
    {
        Snooper::instance()->addProvider($provider);
    }

    /**
     * @param $name
     * @param $params
     * @throws \Exception
     */
    public function raiseEvent(SnooperEvent $event)
    {
        $this->stopwatch->start('Snooper processor');

        Snooper::instance()->setContext($this->container);
        Snooper::instance()->raiseEvent($event->getEventName(),$event->getParameters());

        $this->stopwatch->stop('Snooper processor');
    }

    /**
     * @param RequestEvent $event
     */
    public function onKernelRequest(RequestEvent $event)
    {
        $this->run();

        if ($this->isIgnoredRoute($event->getRequest()->getPathInfo())) {
            return;
        }

        $config = Snooper::instance()->getConfig();
        $session = $this->getSession();

        if ($event->getRequest()->query->has($config->getDebugUrlParam())) {
            $session->set(
                $config->getDebugSessionParam(),
                (bool)$event->getRequest()->query->get($config->getDebugUrlParam())
            );
        }
        if ($session->has($config->getDebugSessionParam())) {
            $config->setDebugMode($session->get($config->getDebugSessionParam()));
        } else if($this->kernel->isDebug()) {
            $config->setDebugMode(true);
        }
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Session\SessionInterface|null
     */
    protected function getSession()
    {
        return $this->requestStack->getMasterRequest()->getSession();
    }

    /**
     * @param ResponseEvent $event
     * @throws \Exception
     */
    public function onKernelResponse(ResponseEvent $event)
    {
        if ($this->isIgnoredRoute($event->getRequest()->getPathInfo())) {
            return;
        }

        if (Snooper::instance()->getUndeliveredResponseCount()) {

            $response = $event->getResponse();

            $xhrSendMode = Snooper::instance()->getConfig()->getXhrSendMode();

            switch (true) {
                case $xhrSendMode === Config::XHR_SEND_METHOD_BODY:
                    if ($response instanceof JsonResponse) {
                        $list = Snooper::instance()->getDeliverResponses();
                        $this->addToJsonContent($response, $list);
                        Container::instance()->addList('body',$list);
                    }
                    break;
                case $xhrSendMode === Config::XHR_SEND_METHOD_HEADER:
                    $list = Snooper::instance()->getDeliverResponses();
                    $responseKey = Snooper::instance()->getConfig()->getJsonResponseKey();
                    $response->headers->set($responseKey, json_encode($list));
                    Container::instance()->addList('head',$list);
                    break;
                case $xhrSendMode === Config::XHR_SEND_METHOD_BOTH:
                    $list = Snooper::instance()->getDeliverResponses();
                    if ($response instanceof JsonResponse) {
                        $this->addToJsonContent($response, $list);
                        Container::instance()->addList('body',$list);
                    }
                    $responseKey = Snooper::instance()->getConfig()->getJsonResponseKey();
                    $response->headers->set($responseKey, json_encode($list));
                    Container::instance()->addList('head',$list);
                    break;
                default:
                    break;
            }
        }

        if ($count = Snooper::instance()->getUndeliveredResponseCount()) {
            LoggerContainer::instance()->add('Processor',sprintf('%s events undelivered remain.',$count));
        }
    }

    /**
     * @param Request $request
     * @return bool
     */
    protected function isIgnoredRoute($currentPath)
    {
        $ignoredRoutes = Snooper::instance()->getConfig()->getIgnoredRoutes();

        foreach ($ignoredRoutes as $ignoredRoute) {
            preg_match($ignoredRoute,$currentPath,$matches);
            if (count($matches)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param JsonResponse $response
     * @param $list
     */
    protected function addToJsonContent(JsonResponse $response,$list)
    {
        $content = $response->getContent();
        $content = json_decode($content);

        switch(true) {
            case is_array($content):
                $this->addToArray($content,$list);
                break;
            case $content instanceof \stdClass:
                $this->addToStdClass($content,$list);
                break;
            default:
                break;
        }

        $response->setData($content);
    }

    /**
     * @param $content
     * @param $list
     */
    protected function addToArray(&$content,$list)
    {
        $responseKey = Snooper::instance()->getConfig()->getJsonResponseKey();

        if (array_key_exists($responseKey,$content)) {
            $content[$responseKey] =
                array_merge(
                    array_values($content[$responseKey]),
                    array_values($list)
                );
        } else {
            $content[$responseKey] = $list;
        }
    }

    /**
     * @param $content
     * @param $list
     */
    protected function addToStdClass(&$content,$list)
    {
        $responseKey = Snooper::instance()->getConfig()->getJsonResponseKey();

        if (property_exists($content, $responseKey)) {
            $content->{$responseKey} =
                array_merge(
                    array_values($content->{$responseKey}),
                    array_values($list)
                );
        } else {
            $content->{$responseKey} = $list;
        }
    }
}
