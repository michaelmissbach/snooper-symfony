# Snooper Bridge Bundle
## Setup project
### Install (SF3)
To install via composer, add the following line to your composer.json in your project root path:

```json
{
    ...
    require: {
    ...
    "snooper/symfony": "1.*"
    }
    ...
}
```

Execute composer:

```
composer update
```

You should see something similar:

```
Loading composer repositories with package information
Updating dependencies (including require-dev)
Package operations: 1 install, 0 updates, 0 removals
Package operations: 1 install, 0 updates, 0 removals
  - Installing snooper/symfony (1.0.0): Loading from ...
```

After the process finished, add the following line to your AppKernel.php:

```php
class AppKernel extends Kernel
{
    ...
    public function registerBundles()
    {
        $bundles = [
            ...
            new Snooper\SnooperBridgeBundle\SnooperBridgeBundle()
        ];
    }
    ...
}
```

As the last step, you should clear your cache and there you are!

### Prepare templates
Add the twig function calls like it´s shown below, depending on your snooper config:
```html
<!DOCTYPE html>
<html>
    <head>
        ...
        {{ print_snooper_section('head') }}
    </head>
    <body>
        {{ print_snooper_section('body') }}
        ...
        Content goes here!
        ...
        {{ print_snooper_section('footer') }}
    </body>
</html>
```

Hint: The tracking informations are added automatically, if your controller returns a "JsonResponse" object.

## Config
To modify the Config, like it is described in the component readme, in symfony context there is a dispatched event, called "snooper.config". Add to your service-config:
```yaml
services:
    ...
    tracking-event.listener:
        class: App\EventListener
        tags:
            - { name: kernel.event_listener, event: snooper.config }
    ...
```
```php
<?php
namespace App;

use Snooper\SnooperBridgeBundle\Event\SnooperConfigEvent;

class EventListener
{
    public function onSnooperConfig(SnooperConfigEvent $event)
    {
        $config = $event->getConfig();
        $config->mergeConfig(['first_section'=>'head']);
    }
}
```

## Implement
### Setup the first event
First you have to create an event call:
```php
<?php
namespace ...;

use Snooper\SnooperBridgeBundle\Event\SnooperEvent;
use Snooper\SnooperBridgeBundle\EventListener\Events;

class DefaultController extends Controller
{
    public function defaultAction(Request $request)
    {
        $this->get('event_dispatcher')->dispatch(Events::SNOOPER_EVENT,SnooperEvent::create('some.event',['some_variable'=>'hello world']));
        ...
    }
}
```
Now every available provider can listen to this event, called "some.event". The second parameter of the EventTrigger is used dynamically. Everything entered will be sent "one-to-one" to the providers. There are no restrictions of data you like to send.

### Create a provider
Now you need to implement a listener for the event. Every provider can listen to several events. Create as it explained in snooper component documentation.

Create a service definition for usage in symfony:
```yml
services:
    ...
    tracking_provider.test:
        class: App\Providers\TestProvider
    ...
```
The tracking service need to know about your new provider, too. For this, just add a tag to the definition. It ends like this:
```yml
services:
    ...
    tracking_provider.test:
        class: App\Providers\TestProvider
        tags:
            - { name: snooper_provider }
    ...
```
Hint: Feel free to inject any service you like through the constructor because it isn´t used by the process. Don´t forget to clear the cache.

## Debug

Add "&snooper_debug=1" to your url to turn it on and "&snooper_debug=0" to turn it off again. The debug mode are stored in session. So there is no need to add it on every url.
